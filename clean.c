#include <stdio.h>
#include <dirent.h>
#include <string.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <time.h>

int main(int argc, char *argv[])
{	
	if(argc!=1) 
	return 1;

	char *path = "."; //current working directory
	DIR *dir = opendir(path);

	if(dir==NULL){
		fprintf(stderr, "Cannot open dir %s: %s\n", path, strerror(errno));
		     return 1;
	} 

	struct dirent *entry;
	while((entry=readdir(dir))!=NULL){
		printf("%ld %s\n", entry->d_ino, entry->d_name);
	}

	return 0;
}


