#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>

int main(int argc, char *argv[])
{
	char *filename = argv[1];
        int input = open(filename, O_RDONLY);
        char c;
        while(1){
                int r = read(input,&c,1);
                if(r==0)
                        break;
                write(1,&c, 1);
        }
	close(input);
        return 0;
}


