#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <string.h>


int main(int argc, char *argv[])
{

	char *filename = argv[1];
	int input = open(filename, O_RDONLY);
	
	if(input==-1){
                printf("Cannot open file %s: %s\n",filename, strerror(errno));
                return 1;
        }

	char c;
	int count = 0;
	while(1){
		int r;
		r = read(input,&c,1);
		if (c=='\n')
			count++;	
		if(r == 0)
			break;
		write(1,&c, 1);
		if(count==10)
			break;
 	}
	return 0;
}

