
#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <string.h>
#include <stdlib.h>

int main(int argc, char *argv[])
{

	char *filename = argv[1];
	int input = open(filename, O_RDONLY);
	
	if(input==-1){
                printf("Cannot open file %s: %s\n",filename, strerror(errno));
                return 1;
        }

	char c;
	int count = 0;
	int slen;
	while(1){
		int r;
		r = read(input,&c,1);
		if (c=='\n')
			count++;	
		if(r == 0){
			slen = count;
			break;
			}
 		}

	count = 0;
	lseek(input,0,SEEK_SET);

	if((slen-11)<0) {
	printf("Warning: File is not long enough to support tail printing all lines\n");
		while(1) {
			int r2;
			r2 = read(input,&c,1);
			if (c=='\n')
				count++;
			if(r2 == 0)
				break;
			write(1,&c,1);
			}
	exit(1);
	}
	while(1){
		int r1;
		r1 = read(input,&c,1);
		if (c=='\n')
			count++;
		if(r1==0)
			break;
		if(count >= (slen-11))
			write(1,&c,1);
		}
	return 0;
}

