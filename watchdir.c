#include <stdio.h>
#include <dirent.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <stdlib.h>

int main(int argc, char *argv[])
{	
	if(argc!=2) 
		return 1;

	//malloc to arrays of directory entry
	struct dirent **old = malloc(sizeof(struct dirent *) *100);
        struct dirent **new = malloc(sizeof(struct dirent *) *100);

	int old_count, new_count;

	char *path = argv[1];
	
	/*Read in the snapshot of the directory
	Store all entries in old  array */
	DIR *dir = opendir(path);
        if(dir==NULL){
		fprintf(stderr, "Cannot open dir %s: %s\n", path, strerror(errno));
                return 1;
        }
	//number of entries
	old_count=0;
	while(1){
		old[old_count] = readdir(dir);
		if(old[old_count]==NULL)
			break;
	
                //add printf to print out the inode # and filename here to see the content
		old_count++;
	}

	closedir(dir);

	while(1){
		sleep(1);
	        /*Read in the new  snapshot of the directory
	        Store all entries in new array */
	
		dir = opendir(path);
		if(dir==NULL){
			fprintf(stderr, "Cannot open dir %s: %s\n", path, strerror(errno));
		     return 1;
		} 
		//number of entries
		new_count = 0;
		while(1){
			new[new_count] = readdir(dir);
			if(new[new_count]==NULL)
				break;
	                //add printf to print out the inode # and filename here to see the content
			new_count++;
		}

		closedir(dir);

		/*Need to compare the new snapshot with the old snapshot
		Probally need a few nested for loops here
		To check for new file(s), deleted file(s) and file(s) with new name
		*/

		int i,j;

		//this nested for loop will detect new files
		//start with the new array and search the old array for the inode number
		//if the inode number is not in the old array, we have a new file
		for(i=0;i<new_count;i++){
			int found = 0;
			for(j=0; j<old_count; j++){
				if(new[i]->d_ino == old[j]->d_ino){
					found = 1;
					break;
				}
			}
			if(found == 0){
				printf("file %s was added\n", new[i]->d_name);
			}
		}




	
		/*Before the next iteration
		Need to swap the snapshot, the new snapshot is now old
		*/
		struct dirent **tmp = old;
		old = new;
		new = tmp;
		old_count = new_count;

	}

	return 0;
}


