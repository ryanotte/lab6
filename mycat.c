#include <stdio.h>
#include <unistd.h>
#include <string.h>

int
main (int argc, char *argv[])
{
  char c;
  while (1)
    {
      int r = read (0, &c, 1);
      if (r == 0)
	{
	  break;
	}
      else if ((int) c >= 97 && (int) c <= 122)
	{
	  c = (int) c - 32;
	}
      write (1, &c, 1);
    }
  return 0;
}
