#include <stdio.h>
#include <dirent.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <stdlib.h>

int main(int argc, char *argv[])
{	
	if(argc!=2) 
		return 1;

	//malloc to arrays of directory entry
	struct dirent **old = malloc(sizeof(struct dirent *) *100);
        struct dirent **new = malloc(sizeof(struct dirent *) *100);

	int old_count, new_count;

	char *path = argv[1];
	
	/*Read in the snapshot of the directory
	Store all entries in old  array */
	DIR *dir = opendir(path);
        if(dir==NULL){
		fprintf(stderr, "Cannot open dir %s: %s\n", path, strerror(errno));
                return 1;
        }
	//number of entries
	old_count=0;
	while(1){
		old[old_count] = malloc(sizeof(struct dirent));
                struct dirent *tmp = readdir(dir);
                if(tmp==NULL)
                        break;
                
                old[old_count]->d_ino = tmp->d_ino;
                int k = 0;
                for(k=0;k<strlen(tmp->d_name);k++){
                       old[old_count]->d_name[k] = tmp->d_name[k];
                }

                
                //add printf to print out the inode # and filename here to see the content
                old_count++;
	}


	closedir(dir);

	while(1){
		sleep(1);
	        /*Read in the new  snapshot of the directory
	        Store all entries in new array */
	
		dir = opendir(path);
		if(dir==NULL){
			fprintf(stderr, "Cannot open dir %s: %s\n", path, strerror(errno));
		     return 1;
		} 
		//number of entries
		new_count = 0;
		while(1){
			new[new_count] = malloc(sizeof(struct dirent));
	                struct dirent *tmp = readdir(dir);
        	        if(tmp==NULL)
                	        break;
                
	                new[new_count]->d_ino = tmp->d_ino;
        	        int k = 0;
	                for(k=0;k<strlen(tmp->d_name);k++){
        	        	new[new_count]->d_name[k] = tmp->d_name[k];
                	}
                        //add printf to print out the inode # and filename here to see the content
                        new_count++;

		}

		closedir(dir);

	/*Need to compare the new snapshot with the old snapshot
		Probally need a few nested for loops here
		To check for new file(s), deleted file(s) and file(s) with new name
		*/

		int i = 0;
		int j = 0;
		int found = 0;		
		//This Nested for loop is to detect moved files
			for(j=0;j<old_count;j++){
				for(i=0; i<new_count; i++){
					if(new[i]->d_ino == old[j]->d_ino){//Check if Inodes are the same
						//printf("%ld %ld\n",new[i]->d_ino,old[j]->d_ino);
						if(strcmp(new[i]->d_name,old[j]->d_name)!=0){//Check names are different
							printf("File %s was renamed %s\n", old[j]->d_name, new[i]->d_name);
						}
						break;
					}
				}
			}
		//this nested for loop will detect new files
		//start with the new array and search the old array for the inode number
		//if the inode number is not in the old array, we have a new file
		i = 0;
		j = 0;
			for(i=0;i<new_count;i++){
				found = 0;
				for(j=0; j<old_count; j++){
					if(new[i]->d_ino == old[j]->d_ino){//Check if Inodes are the same
						found = 1;//Not a new file if same Inodes
						break;
					}
				}
				if(found == 0){
					printf("New file %s was added.\n", new[i]->d_name);
				}
			}
		//This Nested for loop is to detect deleted files
		//Currently prints if new file
		i=0;
		j=0;
			for(j=0;j<old_count;j++){
				int gone = 0;
				for(i=0;i<new_count;i++){
					if(new[i]->d_ino == old[j]->d_ino){
						gone = 1;
						break;
					}
				}
				if(gone==0){
					printf("File %s was deleted\n", old[j]->d_name);//Trying to reference old[j]->d_name causes segmentation fault
				}
			}


		



	
		/*Before the next iteration
		Need to swap the snapshot, the new snapshot is now old
		*/
		struct dirent **tmp = old;
		old = new;
		new = tmp;
		old_count = new_count;

	}

	return 0;
}

